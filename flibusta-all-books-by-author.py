import requests
import bs4
import re
import os

author_page_url = input("\nВведите URL автора на flibusta: ");
print('Получаю данные...')
result  = requests.get(author_page_url)
soup = bs4.BeautifulSoup(result.text, 'lxml')

while 'Отметить выбранное как прочитанное' not in str(soup):
    print('Не удаётся открыть страницу...')
    author_page_url = input("\nВведите URL автора на flibusta: ")
    print('Получаю данные...')
    result  = requests.get(author_page_url)
    soup = bs4.BeautifulSoup(result.text, 'lxml')
  
soup = bs4.BeautifulSoup(result.text, 'lxml')
author_name = soup.select('.title')[0].get_text()
print (author_name)

book_format = input('\nВведите формат (fb2, mobi, epub): ')
while book_format.lower() not in ['fb2','mobi','epub']:
    print('Неверный формат.')
    book_format = input('\nВведите формат (fb2, mobi, epub): ')
    
print('\nПожалуйста подождите...')

form = str(soup.select('form')[1])
links = re.findall(r'<a href=\"/b/\d+\">.*?<span', form)
books = []
for l in links:
    split_link = l.split('</a>')
    split_id_and_name = split_link[0].split('>')
    book_id = split_id_and_name[0][12:-1]
    book_name = split_id_and_name[1]
    if len(split_link) > 2:
        href = re.search(r'<a href=\"/a/\d+\">', split_link[1]).group(0)
        extras = split_link[1].replace(href, '')
        book_name += extras
    book_name += split_link[-1][:-5]
    corrected_book_name = book_name.translate ({ord(c): '' for c in "*:/<>?\|`"})
    books.append((corrected_book_name[:-1], book_id))

split_url = author_page_url.split('/')
flibusta_url = f'{split_url[0]}//{split_url[2]}'
folder_name = f'{author_name} - все книги с flibusta в формате {book_format}'
os.mkdir(folder_name)

for book in books:
    book_url = flibusta_url + '/b/' + book[1] + '/' + book_format
    print(f'\nНачинаю скачивание книги "{book[0]}".')
    book_file = requests.get(book_url)
    extension = book_format
    if book_format.lower() == 'fb2':
        extension = 'zip'
    local_file = os.getcwd() + '\\' + folder_name + '\\' + book[0] + '.' + extension
    print(f'Сохраняю в {local_file}.')
    f = open(local_file,'wb')
    f.write(book_file.content)
    f.close()
    print('Файл сохранён.')
    
print('\nЗагрузка завершена')